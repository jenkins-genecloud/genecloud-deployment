import argparse
import requests

services = {
    "EIM": ("https://eim.dev.genecloud.com", 401),
    "EM": ("https://em.dev.genecloud.com", 404),
    "LEDGER": ("https://ledger.dev.genecloud.com", 404),
    "GFS": ("https://gfs.dev.genecloud.com", 404)
}


def check_health(service_url, status_code):
    response = requests.get(service_url)
    if response.status_code == status_code: return True
    return False


parser = argparse.ArgumentParser(description='Genecloud stack health check script')
parser.add_argument('-s', '--services', nargs='+', help='List of urls to check health on', default=["EIM", "EM"])
args = parser.parse_args()

for service in services:
    endpoint, expected_response = services.get(service)
    outcome = check_health(endpoint, expected_response)
    if not outcome: print "Healthcheck failed for service: %s at endpoint: %s" % (service, endpoint)

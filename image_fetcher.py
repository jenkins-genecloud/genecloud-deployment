import json
import argparse
import requests
from distutils.version import LooseVersion


def fetch_latest(image_name):
    response = requests.get("https://%s/v2/%s/tags/list" % (args.docker_host, image_name)).json()
    tags = response.get('tags', [])
    versions = [LooseVersion(x) for x in tags]
    versions.sort()
    latest = versions[-1].vstring if len(versions) > 0 else 0
    if latest == 0: raise Exception("Failed to fetch version from repository: %s" % image_name)
    print("Latest version in repository for image: %s is %s" % (image, latest))
    return latest


parser = argparse.ArgumentParser(description='CloudFormation configuration updater')
parser.add_argument('-o', '--output', help='Output path', type=argparse.FileType('w'), required=True)
parser.add_argument('-t', '--template', help='Input template path', type=argparse.FileType('r'), required=True)
parser.add_argument('-d', '--docker_host', help='Docker repository url', default="docker.genecloud.com")
parser.add_argument('-i', '--images', nargs='+', help='List of images to query',
                    default=["experiment-instance-manager",
                             "experiment-manager",
                             "genecloud-file-store",
                             "ledger",
                             "authx-io"])
args = parser.parse_args()

versions = dict([(args.docker_host + '/' + image, fetch_latest(image)) for image in args.images])

template = {}

with args.template as cf_template:
    template = json.load(cf_template)
    for entry in template:
        param_value = entry.get("ParameterValue", "")
        image = param_value.split(':')[0]
        if param_value.startswith(image) and versions.has_key(image):
            entry["ParameterValue"] = image + ':' + versions[image]

with args.output as output_file:
    json.dump(template, output_file, indent=4, sort_keys=True)
